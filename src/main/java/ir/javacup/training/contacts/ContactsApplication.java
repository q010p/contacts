package ir.javacup.training.contacts;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.core.env.Environment;

import java.net.InetAddress;
import java.net.UnknownHostException;

@SpringBootApplication
public class ContactsApplication {

    private static final Logger logger = LoggerFactory.getLogger(ContactsApplication.class);

    public static void main(String[] args) throws UnknownHostException {
        SpringApplication springApplication = new SpringApplication(ContactsApplication.class);
        Environment env = springApplication.run(args).getEnvironment();
        String protocol = "http";
        logger.info("\n######################################################\n\t" +
                        "Application '{}' is running! Access URLs:\n\t" +
                        "Local: \t\t{}://localhost:{}\n\t" +
                        "External: \t{}://{}:{}\n\t" +
                        "Profile(s): \t{}\n######################################################",
                env.getProperty("spring.application.name"),
                protocol,
                env.getProperty("server.port"),
                protocol,
                InetAddress.getLocalHost().getHostAddress(),
                env.getProperty("server.port"),
                env.getActiveProfiles());
    }
}
