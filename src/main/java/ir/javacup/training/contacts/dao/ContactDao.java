package ir.javacup.training.contacts.dao;

import ir.javacup.training.contacts.entities.ContactEntity;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Named("contactDao")
public class ContactDao extends AbstractJpaDAO<ContactEntity, Integer> {

    @PersistenceContext
    private EntityManager entityManager;


    public ContactEntity load(Long id) {
        // TODO implement this method
        return null;
    }

    public List<ContactEntity> findAll() {
        // TODO implement this method
        return null;
    }

    @Override
    public Class<ContactEntity> getEntityClass() {
        return ContactEntity.class;
    }

}
