package ir.javacup.training.contacts.dao;


import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;

/**
 * an abstract class for basic database functions
 */
abstract class AbstractJpaDAO<T, U extends Serializable> {

    @PersistenceContext
    private EntityManager entityManager;

    public void save(T entity) {
        entityManager.persist(entity);
    }

    public T findById(U id) {
        return entityManager.find(getEntityClass(), id);
    }

    public abstract Class<T> getEntityClass();

}
