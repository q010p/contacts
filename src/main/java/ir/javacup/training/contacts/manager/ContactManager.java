package ir.javacup.training.contacts.manager;

import ir.javacup.training.contacts.dao.ContactDao;
import ir.javacup.training.contacts.entities.ContactEntity;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import java.util.List;

@Named("contactManager")
public class ContactManager {
    private final
    ContactDao dao;

    public ContactManager(ContactDao dao) {
        this.dao = dao;
    }

    @Transactional
    public void save(ContactEntity entity) {
        dao.save(entity);
    }

    public ContactEntity load(Long id) {
        // TODO implement this method
        return null;
    }

    public List<ContactEntity> findAll() {
        // TODO implement this method
        return null;
    }
}
