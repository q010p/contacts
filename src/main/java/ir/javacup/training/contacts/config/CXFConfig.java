package ir.javacup.training.contacts.config;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import ir.javacup.training.contacts.services.ContactService;
import ir.javacup.training.contacts.services.impl.ContactServiceImpl;
import org.apache.cxf.Bus;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.inject.Inject;
import java.util.Arrays;

/**
 * cxf configuration
 */

@Configuration
class CXFConfig {
    private final Bus bus;

    private final
    ContactService contactService;


    @Inject
    public CXFConfig(Bus bus, ContactService contactService) {
        this.bus = bus;
        this.contactService = contactService;
    }


    @Bean
    public Server rsServer() {
        final JAXRSServerFactoryBean endpoint = new JAXRSServerFactoryBean();
        endpoint.setProvider(new JacksonJsonProvider());
        endpoint.setBus(bus);
        endpoint.setServiceBeans(Arrays.asList(contactService));
        endpoint.setAddress("/");
        return endpoint.create();
    }


}