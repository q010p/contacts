package ir.javacup.training.contacts.entities;

import javax.persistence.*;

@Entity
@Table(name="WISE_CONTACT")
public class ContactEntity {

    private Long id;
    private String name;

    @Id
    @Column(name = "CONTACT_ID")
    @GeneratedValue(strategy=GenerationType.AUTO)
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name="NAME_")
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}
