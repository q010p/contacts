package ir.javacup.training.contacts.services.impl;

import ir.javacup.training.contacts.entities.ContactEntity;
import ir.javacup.training.contacts.services.ContactService;

import javax.inject.Named;

@Named("contactService")
public class ContactServiceImpl implements ContactService {
    @Override
    public ContactEntity load(Long id) {
        // TODO implement this method
        ContactEntity entity = new ContactEntity();
        entity.setId(100L);
        entity.setName("Dummy Contact");
        return entity;
    }

    @Override
    public Long save(ContactEntity entity) {

        return null;
    }
}
