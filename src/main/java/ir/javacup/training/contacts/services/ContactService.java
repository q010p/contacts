package ir.javacup.training.contacts.services;


import ir.javacup.training.contacts.entities.ContactEntity;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/contact")
public interface ContactService {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/load/{pk}")
    public ContactEntity load(@PathParam("pk") Long id);

    @POST
    @Path("/save")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Long save(ContactEntity entity);


}
